\documentclass[11pt, numbers=endperiod, parskip=half]{scrartcl}

\usepackage{minted}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{abstract}
\usepackage{geometry}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{hyperref}

\title{Assignment 1 - Adaptive Random Testing}
\subtitle{SWE40004 - Software Testing and Reliability}
\author{Daniel Parker - 971328X}

\date{\today}

\begin{document}
\maketitle

\begin{abstract}
This report covers the process of randomly testing software with a known block failure region. Namely, the goal was to compare the effectiveness of testing with inputs selected using \textit{Fixed Size Candidate Set Adaptive Random Testing} compared to just randomly selected inputs. F-Measure and average running time before a failure input is found and used to compare the two test case selection strategies.
\end{abstract}

\section{Experiment}
\subsection{Input Domain, Failure Region and Failure Rate}
We define the input domain, \textit{D} to be the valid range of input \textit{x} and \textit{y} values. The failure region, \textit{m} is the range of failure inputs within the input domain. For this experiment, we define a block pattern failure region rather than a Point or Strip pattern. The failure rate \(\theta\) is defined as \(\frac{m}{d}\). In this experiment we would like observe the effectiveness of random testing on a fixed failure rate of 0.001 so we design a Failure Region to Input Domain size ratio that ensures that failure rate.

\subsection{Selected Regions}
In the tests that yielded the results in section 3, the Input Domain was \(0 <= x <= 500\) and \(0 <= y <= 200\) where \(x, y \in \mathbb{R}\). The failure region was defined to be \(0 <= x <= 10\) and \(0 <= y <= 10\). The failure rate is therefore \(\frac{area(f-region)}{area(domain)} = \frac{10 \times 10}{500 \times 200} = \frac{100}{100000} = 0.001\)

\subsection{Test function}
For the purpose of testing, a fabricated erroneous function is created. The function will accept inputs \textit{x} and \textit{y} which are Real numbers and within the valid input domain. The function checks if the input point is within the failure region and returns \textbf{false} if it is or \textbf{true} if it isn't. This is sufficient enough to test the effectiveness of FSCS-ART and RT test strategies.

\section{Program}
\centering{
	\textit{All source code referenced in this section can be found in section 4.}
}

\raggedright
The Java programming language was chosen to implement this test program due to the availability of third-party libraries for CSV writing. The language is also an object-oriented language and lends itself well to writing software such as this program.

\subsection{Structure}
The program is split into logical sections;

\begin{longtable}{|p{0.3\linewidth}|p{0.7\linewidth}|}
	\hline
	\textbf{function/class}		&	\textbf{role}\\ \hline
	\textit{main()}				&	Initialises all csv related data and performs formatted terminal output. Main calls the \textit{FSCSART()} and \textit{RT()} methods which perform their respective test strategy. \\ \hline
	\textit{FSCSART()}			&	Executes all the iterations of FSCS testing and collects summary data. Returns a Summary object on completion\\ \hline
	\textit{runFSCSARTTest()}	&	Execute a single FSCS-ART test using the \textit{generateTests()} and \textit{selectTest()} methods\\ \hline
	\textit{generateTests()}	&	A candidate set of size \textit{k} test inputs is generated that are all unique from each other and past executed test points\\ \hline	
	\textit{selectTest()}		&	The Euclidean distance between each candidate and all previous executed test points is calculated and then the point furthest from those is selected as the next FSCS-ART test input.\\ \hline
	\textit{RT()}				&	Executes all the iterations of Random Testing and collects summary data. Returns a Summary object on completion\\ \hline
	\textit{runRandomTest()}	&	Execute a single Random Test with random test case selection only. \\ \hline
	\textit{Point2D}			&	The Point2D class encapsulates the \textit{x} and \textit{y} points for a single test case. Two constructors a provided, one for completely random test generation and another for unique test generation based on past tests and other candidate tests.\\ \hline
	\textit{Summary}			&	The Summary class encapsulates the test summary data; a list of F-Counts and Test failure times for past tests. It also provides methods to retrieve the F-Measure and average test failure times.\\ \hline
	\textit{TestFailedException}&	Custom exception class which is thrown by a failed test result to signify the end of a test round.\\ \hline
	\textit{testDomain()}		&	The function which is used for testing. This function takes \textit{x} and \textit{y} as parameters and returns \textbf{false} for a failure or \textbf{true} for a success.\\ 
	\hline
\end{longtable}

\subsection{Execution Instructions}
\begin{enumerate}
	\item{Install the JDK 1.7 from the \textcolor{blue}{\href{http://www.oracle.com/technetwork/java/javase/downloads/jdk7-downloads-1880260.html}{Java website}}.}
	\item{Unzip the supplied source into a directory and open a terminal at that location.}
	\item{Execute the packaged jar archive using \textbf{java -jar art.jar}}
	\item{Alternatively import the directory as a project into IntelliJ IDEA and change the source how you like. \textcolor{blue}{\href{http://www.jetbrains.com/idea/download/}{Free download here}}.}
\end{enumerate}

\section{Results}
\subsection{Measures}
\raggedright
There are two main measures which are considered important for the results of these tests. The first is the F-Measure, which is the average of F-Counts (the number of test input before a failure causing input). The other is the average time to find a failure causing input in milliseconds, which we call the T-Measure.

\begin{align*}
	FSCS-ART\ Experimental\ F-Measure &= 341\ tests\\
	FSCS-ART\ Experimental\ T-Measure &= 215.0\ ms\\
	\\
	RT\ Experimental\ F-Measure &= 1009\ tests\\
	RT\ Experimental\ T-Measure &=\ < 0.0\ ms\\
\end{align*}
\subsection{Charts}
\setlength\fboxsep{0pt}
\setlength\fboxrule{0.5pt}
\centering{
	\fbox{\includegraphics[width=12cm]{fcounts.png}}
}\\
\centering{
	\fbox{\includegraphics[width=12cm]{times.png}}
}\\

\subsection{Comments}
\raggedright
Close to completing this report, the test was run outside of the IntelliJ IDE that was used for the results above, giving extremely better performance for FSCS than inside the IDE. The output of that execution is given below. Another key issue with the test execution is that in retrospect the time of execution should have been recorded in nanoseconds instead of milliseconds so that RT time would not come out at 0.0ms. When compared to FSCS-ART it is not an issue as FSCS-ART takes significantly longer in comparison to find the first failure.

\centering{
	\fbox{\includegraphics[width=\linewidth]{jartest.png}}
}\\

\subsection{Analysis}
\raggedright
From the results, it's evident that the F-Measure is significantly less for FSCS-ART testing than RT testing. FSCS-ART testing on average will find the failure causing input in one third of the time that random testing will. This means that the process of selecting testing input has been improved by FSCS-ART. However, the time to find a failure causing input ends up being much larger for FSCS-ART than random testing. This is due to the increase in operations and resource required to select an appropriate test input for FSCS-ART, which RT does not mandate.

To calculate the (time-independent) performance improvement made by FSCS over RT the formula below can be used.

\[
	\frac{\textit{F-Measure(RT)} - \textit{F-Measure(FSCS-ART)}}{\textit{F-Measure(RT)}} \times 100
\]

In this case there is a performance increase of \(66\%\) or \(\frac{2}{3}\) over RT.

\newgeometry{margin=2cm}
\begin{landscape}
\section{Source Code}
\inputminted{java}{../src/au/net/danielparker/Main.java}
\end{landscape}
\restoregeometry

\section{References}
\begin{enumerate}
	\item{Chen T.Y., Leung H., Mak I.K. 2005, \textit{Adaptive Random Testing}, Swinburne Library Online}
\end{enumerate}

\end{document}
