\documentclass[11pt, numbers=endperiod, parskip=half]{scrartcl}

\usepackage{minted}
\usepackage{color}
\usepackage{pdflscape}
\usepackage{abstract}
\usepackage{geometry}
\usepackage{amsfonts}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{longtable}
\usepackage{hyperref}

\title{Assignment 2}
\subtitle{SWE40004 - Software Testing and Reliability}
\author{Daniel Parker - 971328X}

\date{\today}

\begin{document}
\maketitle

\begin{abstract}
	The purpose of this report is to show the findings of extended work on the
	previous findings regarding the effectiveness of Random Testing (RT) and
	Fixed Size Candidate Set Adaptive Random Testing (FSCS-ART). This report poses
	the situation where an error-free subject program is taken as a control, then
	three mutated programs are created. Each program is tested by generating
	random tests as per the RT or FSCS-ART methods and then comparing program
	result to the original program's result.
\end{abstract}

\section{Experiment}
\subsection{Input Domain, Failure Region and Failure Rate}
As in the previous report, we define the input domain, \textit{D} to be the
valid range of input \textit{x} and \textit{y} values. The failure region,
\textit{m} is the range of failure inputs within the input domain. In this
experiment, the failure regions are block regions. The failure rate \(\theta\)
is defined as \(\frac{m}{d}\). The failure rates will be different across the
three mutated programs and are covered in those sections.

\subsection{Random Testing Details}
Both RT and FSCS-ART were run for 2000 iterations per faulty program using a
fix seeded random number generator for the entire of the iterations. The FSCS-ART
fixed candidate set size is 10 and are guarunteed to be unique between themselves
and past test points.

\subsection{Selected Regions}
In the tests that yielded the results in section 3, the Input Domain was
\(0 <= x <= 1000\) and \(0 <= y <= 1000\) where \(x, y \in \mathbb{R}\). Each
program mutation has a different failure region and failure ratio. They are
reported in their respective sections.

\subsubsection{Faulty Program 1}
The failure regions was defined to be \(500 <= x <= 520\) and \(0 <= y <= 1000\).
The failure rate is therefore \(\frac{area(f-region)}{area(domain)} = \frac{20
\times 1000}{1000 \times 1000} = \frac{20000}{1000000} = 0.02\)

\subsubsection{Faulty Program 2}
The failure regions was defined to be \(0 <= x < 500\) and \(500 <= y < 950\).
The failure rate is therefore \(\frac{area(f-region)}{area(domain)} = \frac{500
\times 450}{1000 \times 1000} = \frac{225000}{1000000} = 0.225\)

\subsubsection{Faulty Program 3}
The failure regions was defined to be \(500 <= x <= 1000\) and \(0 <= y <= 1000\).
The failure rate is therefore \(\frac{area(f-region)}{area(domain)} = \frac{500
\times 1000}{1000 \times 1000} = \frac{500000}{1000000} = 0.5\)

\subsection{Test functions}
There are four test functions, where three of them are mutations on a control function.
The functions take two arguments; x and y of type float and return an enum
value of type Sector. Sector is just an abstraction on the physical sectors,
which have been programmed into the logic of the functions.

There are six non-equal sectors, which all lie in the input domain
\textit{X\_RANGE\_MIN}
to \textit{X\_RANGE\_MAX} and \textit{Y\_RANGE\_MIN} to \textit{Y\_RANGE\_MAX}. The
configuration of these sectors is like a table of two rows (x) and 3 columns (y).

\textit{See source code sections 4.1 and 4.5}

\section{Program}
\centering{
	\textit{All source code referenced in this section can be found in section 4.}
}

\raggedright
The program and testing are written in the Python programming language. The
source code for RT and FSCS-ART are ported from the original Java version. The
reason for change, is that the Python source code is a little more concise and
allows the reader to understand the program's purpose more clearly.

\subsection{Structure}
The program is split into two packages. The package \textit{Programs} contains
the programs being tested and the \textit{Point2D} class. The package
\textit{Test} contains all the unit test code including the \textit{RT} and
\textit{ART} functions. Outside of both packages and shared between them is a
source file, \textit{const.py} containing the program constants.

\subsubsection{Programs Package}
\begin{longtable}{|p{0.2\linewidth}|p{0.8\linewidth}|}
	\hline
	\textbf{function/class}		&	\textbf{role}\\ \hline
	\textit{Original.py}			&	The original program with error-free get\_sector() function.\\ \hline
	\textit{Faulty1.py}			&	Mutated copy of original program.\\ \hline
	\textit{Faulty2.py}			&	Mutated copy of original program.\\ \hline
	\textit{Faulty3.py}			&	Mutated copy of original program.\\ \hline
	\textit{Point2D}			&	The Point2D class encapsulates the \textit{x} and \textit{y} points for a single test case. Two constructors are provided, one for completely random test generation and another for unique test generation based on past tests and other candidate tests.\\
	\hline
\end{longtable}

\subsubsection{Test Package}
\begin{longtable}{|p{0.2\linewidth}|p{0.8\linewidth}|}
	\hline
	\textbf{function/class}		&	\textbf{role}\\ \hline
	\textit{test\_all.py}	&	Defines unit test specs for each RT and FSCS-ART test of the three mutated programs. \\ \hline
	\textit{ART.py}				&	Executes all the iterations of FSCS testing and prints progress and results of testing. \\ \hline
	\textit{RT.py}				&	Executes all the iterations of Random Testing and prints progress and results of testing. \\
	\hline
\end{longtable}

\subsection{Execution Instructions}
\begin{enumerate}
	\item{Install Python 2.7 via your system's correct installation channels on the \textcolor{blue}{\href{https://www.python.org/download/releases/2.7.8/}{Python website}}.}
	\item{Install Python pip (required for enum backports) \textcolor{blue}{\url{http://pip.readthedocs.org/en/latest/installing.html\#install-pip}}.}
	\item{Install the enum backports from Python 3.4. \textbf{sudo pip install enum34}}
	\item{Unzip the supplied source into a directory and open a terminal at that location.}
	\item{Execute the unit tests using discover mode \textbf{python -m unittest discover}}
	\item{Alternatively import the directory as a project into IntelliJ PyCharm and execute the unit tests from there. \textcolor{blue}{\href{https://www.jetbrains.com/pycharm/download/}{Free download here}}.}
\end{enumerate}

\section{Results}
\subsection{Measures}
\raggedright
There are two main measures which are considered important for the results of
these tests. The first is the F-Measure, which is the average of F-Counts (the
number of test input before a failure causing input). The other is the average
time to find a failure causing input in nanoseconds, which we call the T-Measure.

\subsubsection{Mutant Program 1}
\begin{align*}
	FSCS-ART\ Experimental\ F-Measure &= 45.751\ tests\\
	FSCS-ART\ Experimental\ T-Measure &= 72341854.5\ ns\\
	\\
	RT\ Experimental\ F-Measure &= 48.6365\ tests\\
	RT\ Experimental\ T-Measure &=\ 467596.0\ ns\\
\end{align*}
\subsubsection{Mutant Program 2}
\begin{align*}
	Mutant Program 2
	FSCS-ART\ Experimental\ F-Measure &= 3.45\ tests\\
	FSCS-ART\ Experimental\ T-Measure &= 886005.5\ ns\\
	\\
	RT\ Experimental\ F-Measure &= 4.4195\ tests\\
	RT\ Experimental\ T-Measure &=\ 55294.0\ ns\\
\end{align*}
\subsubsection{Mutant Program 3}
\begin{align*}
	Mutant Program 3
	FSCS-ART\ Experimental\ F-Measure &= 1.572\ tests\\
	FSCS-ART\ Experimental\ T-Measure &= 323532.50\ ns\\
	\\
	RT\ Experimental\ F-Measure &= 1.966\ tests\\
	RT\ Experimental\ T-Measure &=\ 27167.50\ ns\\
\end{align*}

\subsection{Raw Results}
\begin{minted}{text}
FSCS-ART:
    Mutant Program:  Faulty1
		fMeasure:  45.751
		Average time to fail:  72341854.5  ns
.FSCS-ART:
    Mutant Program:  Faulty2
		fMeasure:  3.45
		Average time to fail:  886005.5  ns
.FSCS-ART:
    Mutant Program:  Faulty3
		fMeasure:  1.572
		Average time to fail:  323532.499999  ns
.RT:
    Mutant Program:  Faulty1
		fMeasure:  48.6365
		Average time to fail:  467596.0  ns
.RT:
    Mutant Program:  Faulty2
		fMeasure:  4.4195
		Average time to fail:  55293.9999999  ns
.RT:
    Mutant Program:  Faulty3
		fMeasure:  1.966
		Average time to fail:  27167.4999997  ns
.
----------------------------------------------------------------------
Ran 6 tests in 149.888s

OK
\end{minted}

\subsection{Comments}
\raggedright
If the experiment was to be repeated, the program would be written using Python
version 3.4 rather than 2.7. This is due to the significant improvements in
basic mathematics in that version, especially in regards to the division of
integers to produce a float value, rather than floor.

\subsection{Analysis}
\raggedright
The results from this testing approach are in contrast to results found in the
previous comparisons of FSCS-ART and RT. This experiment shows that when the two
random test selection strategies are compared in this way, the FSCS-ART
selection approach doesn't end up being that much more optimal in terms of the
observed F-Measures and extremely slow when comparing the average time to fail.

Once again we calculate the time-independent performance improvements to
compare the two selection approaches. To calculate the (time-independent)
performance improvement made by FSCS over RT the formula below can be used.

\[
	\frac{\textit{F-Measure(RT)} - \textit{F-Measure(FSCS-ART)}}{\textit{F-Measure(RT)}} \times 100
\]

\centering
\begin{longtable}{|p{0.2\linewidth}|p{0.7\linewidth}|}
	\hline
	\textbf{Mutation}		&	\textbf{\% Performance improvement of FSCS-ART over RT}\\ \hline
	\textit{Faulty1}				&	5.93\% \\ \hline
	\textit{Faulty2}				&	21.94\% \\ \hline
	\textit{Faulty3}				& 20.04\% \\
	\hline
\end{longtable}

\raggedright
From these performance improvements one could deduce that FSCS-ART is a more
effective approach for selecting random test imputs, however the key issue in
this case is that the FSCS-ART random tests are taking significantly longer to
actually execute and find a fault than just normal RT selection.

% \newgeometry{margin=2cm}
% \begin{landscape}
\section{Source Code}
\subsection{Original Program}
\inputminted{python}{"../Python Solution/Programs/Original.py"}

\subsection{Faulty1 Program}
\inputminted{python}{"../Python Solution/Programs/Faulty1.py"}

\subsection{Faulty2 Program}
\inputminted{python}{"../Python Solution/Programs/Faulty2.py"}

\subsection{Faulty3 Program}
\inputminted{python}{"../Python Solution/Programs/Faulty3.py"}

\subsection{Constants}
\inputminted{python}{"../Python Solution/const.py"}

\subsection{Point2D}
\inputminted{python}{"../Python Solution/Programs/Point2D.py"}

\subsection{test\_all.py}
\inputminted{python}{"../Python Solution/Test/test_all.py"}

\subsection{ART}
\inputminted{python}{"../Python Solution/Test/ART.py"}

\subsection{RT}
\inputminted{python}{"../Python Solution/Test/RT.py"}
% \end{landscape}
% \restoregeometry

\section{References}
\begin{enumerate}
	\item{Chen T.Y., Leung H., Mak I.K. 2005, \textit{Adaptive Random Testing},
	Swinburne Library Online.}
	\item{Python Software Foundation, 2014, \textit{Python 2.7.8 Documentation},
	Python Software Foundation, viewed on 14 October 2014,
	\textcolor{blue}{
		\textless\hyperref[https://docs.python.org/2/]{https://docs.python.org/2/}\textgreater}.}
\end{enumerate}

\end{document}
