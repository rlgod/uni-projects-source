from enum import Enum

__author__ = 'Daniel Parker'


# Domain range declaration
X_RANGE_MIN = 0
X_RANGE_MAX = 1000
Y_RANGE_MIN = 0
Y_RANGE_MAX = 1000
CANDIDATES = 10
ITERATIONS = 2000
SEED = 100
NANO_IN_SEC = 1000000000


# Program sector constants as enum
class Sector(Enum):
    A = 1
    B = 2
    C = 3
    D = 4
    E = 5
    F = 6