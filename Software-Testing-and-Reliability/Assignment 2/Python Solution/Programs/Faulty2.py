from const import *

__author__ = 'Daniel Parker'


def get_sector(x, y):
    """Faulty2"""
    if X_RANGE_MIN <= x < 0.5 * X_RANGE_MAX:
        if Y_RANGE_MIN <= y < 0.5 * Y_RANGE_MAX:
            return Sector.A
        # Used 0.95 - Y_RANGE_MAX instead of 0.95 * Y_RANGE_MAX
        if 0.5 * Y_RANGE_MAX <= y < 0.95 - Y_RANGE_MAX:
            return Sector.B
        if 0.95 * Y_RANGE_MAX <= y <= Y_RANGE_MAX:
            return Sector.C
    if 0.5 * X_RANGE_MAX <= x <= X_RANGE_MAX:
        if Y_RANGE_MIN <= y < 0.5 * Y_RANGE_MAX:
            return Sector.D
        if 0.5 * Y_RANGE_MAX <= y < 0.95 * Y_RANGE_MAX:
            return Sector.E
        if 0.95 * Y_RANGE_MAX <= y <= Y_RANGE_MAX:
            return Sector.F