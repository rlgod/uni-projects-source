from const import *
import random

__author__ = 'Daniel Parker'


# Point2D represents a point in 2D space. It can be initialised
# using the class methods new_rt_point and new_art_point.
class Point2D:
    def __init__(self, x, y):
        self.x = x
        self.y = y

    # Construct a new Point2D which is completely random
    @classmethod
    def new_rt_point(cls):
        x = random.randint(X_RANGE_MIN, X_RANGE_MAX)
        y = random.randint(Y_RANGE_MIN, Y_RANGE_MAX)
        return cls(x, y)

    # Construct a new Point2D which is random but not a duplicate
    # of past points or current candidates.
    @classmethod
    def new_art_point(cls, past_tests, candidates):
        while True:
            x = random.randint(X_RANGE_MIN, X_RANGE_MAX)
            y = random.randint(Y_RANGE_MIN, Y_RANGE_MAX)

            point = cls(x, y)
            if not (point in past_tests) and not (point in candidates):
                return point