from __future__ import division
import math
import sys
from Programs.Point2D import *
import time

__author__ = 'Daniel Parker'


# Generate a set of candidate points of size const.CANDIDATES.
def generate_candidates(past_points):
    candidates = []
    for i in xrange(0, CANDIDATES):
        candidates.append(Point2D.new_art_point(past_points, candidates))

    return candidates


# Select the best point in candidates set which is furthest from all other candidates
# and also the furthest from past test points.
def select_point(past_points, candidates):
    best_distance = -1.0
    for c in candidates:
        min_distance = sys.float_info.max
        for t in past_points:
            min_distance = min(min_distance,
                               math.sqrt(math.pow((c.x - t.x), 2.0) +
                                         math.pow((c.y - t.y), 2.0)))

        if best_distance < min_distance:
            selected_point = c
            best_distance = min_distance

    return selected_point


# Run FSCS Adaptive Random Testing on original_program and faulty_program
# always comparing the result of each program's execution to find any
# discrepancy. A discrepancy reveals a fault in the faulty_program.
def art(original_program, faulty_program, callback):
    f_counts = []
    times = []
    f_measure_sum = 0
    time_sum = 0

    for i in xrange(0, ITERATIONS):
        callback("\r%d%%" % ((i / ITERATIONS) * 100))
        past_points = []
        fail = False
        tests = 0

        start_time = time.clock()
        while not fail:
            candidates = generate_candidates(past_points)
            p = select_point(past_points, candidates)
            past_points.append(p)
            if original_program(p.x, p.y) != faulty_program(p.x, p.y):
                fail = True

            tests += 1

        end_time = time.clock()

        f_counts.append(tests)
        times.append(end_time - start_time)

    for i in xrange(0, len(f_counts)):
        f_measure_sum += f_counts[i]

    for i in xrange(0, len(times)):
        time_sum += times[i]

    callback("\r")
    print "    Mutant Program: ", faulty_program.__doc__
    print "\tfMeasure: ", f_measure_sum / ITERATIONS
    print "\tAverage time to fail: ", (time_sum / ITERATIONS) * NANO_IN_SEC, " ns"