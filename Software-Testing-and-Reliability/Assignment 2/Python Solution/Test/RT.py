from __future__ import division
from Programs.Point2D import *
import time

__author__ = 'Daniel Parker'


# Test using randomly selected inputs
def rt(original_program, faulty_program, callback):
    f_counts = []
    times = []
    f_measure_sum = 0
    time_sum = 0

    for i in xrange(0, ITERATIONS):
        callback("\r%d%%" % ((i / ITERATIONS) * 100))
        fail = False
        tests = 0

        start_time = time.clock()

        while not fail:
            p = Point2D.new_rt_point()

            if original_program(p.x, p.y) != faulty_program(p.x, p.y):
                fail = True

            tests += 1

        end_time = time.clock()
        f_counts.append(tests)
        times.append(end_time - start_time)

    for i in xrange(0, len(f_counts)):
        f_measure_sum += f_counts[i]

    for i in xrange(0, len(times)):
        time_sum += times[i]

    callback("\r")
    print "    Mutant Program: ", faulty_program.__doc__
    print "\tfMeasure: ", f_measure_sum / ITERATIONS
    print "\tAverage time to fail: ", (time_sum / ITERATIONS) * NANO_IN_SEC, " ns"