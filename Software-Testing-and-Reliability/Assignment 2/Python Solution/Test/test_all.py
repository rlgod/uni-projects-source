import unittest
import Programs.Original as Original
import Programs.Faulty1 as Faulty1
import Programs.Faulty2 as Faulty2
import Programs.Faulty3 as Faulty3
import ART
import RT
import random
import const
import sys

__author__ = 'Daniel Parker'


# We use unit test specs as a method for running RT and FSCS-ART Testing.
# Assertions are not used, however running the tests will still behave like
# normal unit tests, ie. a failure during runtime will cause the test to fail.
class TestAll(unittest.TestCase):
    # Random testing with discrepancy testing
    # between Original and Faulty1 programs
    def test_rt_f1(self):
        print "RT:"
        random.seed(const.SEED)

        def progress_print(data):
            sys.stdout.write(data)
            sys.stdout.flush()

        RT.rt(Original.get_sector, Faulty1.get_sector, progress_print)

    # Adaptive Random Testing testing with discrepancy
    # testing between Original and Faulty1 programs
    def test_art_f1(self):
        print "FSCS-ART:"
        random.seed(const.SEED)

        def progress_print(data):
            sys.stdout.write(data)
            sys.stdout.flush()

        ART.art(Original.get_sector, Faulty1.get_sector, progress_print)

    # Random testing with discrepancy testing
    # between Original and Faulty2 programs
    def test_rt_f2(self):
        print "RT:"
        random.seed(const.SEED)

        def progress_print(data):
            sys.stdout.write(data)
            sys.stdout.flush()

        RT.rt(Original.get_sector, Faulty2.get_sector, progress_print)

    # Adaptive Random Testing testing with discrepancy
    # testing between Original and Faulty2 programs
    def test_art_f2(self):
        print "FSCS-ART:"
        random.seed(const.SEED)

        def progress_print(data):
            sys.stdout.write(data)
            sys.stdout.flush()

        ART.art(Original.get_sector, Faulty2.get_sector, progress_print)

    # Random testing with discrepancy testing
    # between Original and Faulty3 programs
    def test_rt_f3(self):
        print "RT:"
        random.seed(const.SEED)

        def progress_print(data):
            sys.stdout.write(data)
            sys.stdout.flush()

        RT.rt(Original.get_sector, Faulty3.get_sector, progress_print)

    # Adaptive Random Testing testing with discrepancy
    # testing between Original and Faulty3 programs
    def test_art_f3(self):
        print "FSCS-ART:"
        random.seed(const.SEED)

        def progress_print(data):
            sys.stdout.write(data)
            sys.stdout.flush()

        ART.art(Original.get_sector, Faulty3.get_sector, progress_print)


if __name__ == '__main__':
    unittest.main()
