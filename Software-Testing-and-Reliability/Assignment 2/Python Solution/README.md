README
======

## Dependencies
1. Python 2.7.6
2. Python pip
3. enum34 backports ```sudo pip install enum34```

## Usage
1. Extract project archive ```tar -zxvf <archive_name>.tar.gz```
2. Navigate to project root ```cd <archive_name>``` (Note: project root contains
  this ```README.md``` file)
3. Execute unit tests ```python -m unittest discover```

## Example Test Output
```
FSCS-ART:
    Mutant Program:  Faulty1  
    fMeasure:  45.751
	  Average time to fail:  72341854.5  ns
.FSCS-ART:
    Mutant Program:  Faulty2
	  fMeasure:  3.45
	  Average time to fail:  886005.5  ns
.FSCS-ART:
    Mutant Program:  Faulty3
	  fMeasure:  1.572
	  Average time to fail:  323532.499999  ns
.RT:
    Mutant Program:  Faulty1
	  fMeasure:  48.6365
	  Average time to fail:  467596.0  ns
.RT:
    Mutant Program:  Faulty2
	  fMeasure:  4.4195
	  Average time to fail:  55293.9999999  ns
.RT:
    Mutant Program:  Faulty3
	  fMeasure:  1.966
	  Average time to fail:  27167.4999997  ns
.
----------------------------------------------------------------------
Ran 6 tests in 149.888s

OK
```

## Assistance
Contact [@rlgod](http://github.com/rlgod) at [dparker.tech@gmail.com](mailto:dparker.tech@gmail.com)
