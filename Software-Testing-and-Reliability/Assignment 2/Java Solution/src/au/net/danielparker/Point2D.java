package au.net.danielparker;

import java.util.ArrayList;

/**
 * @author Daniel Parker <a href="mailto:dparker.tech@gmail.com">dparker.tech@gmail.com</a>
 */
public class Point2D {
    public double x;
    public double y;

    /**
     *  Default constructor just generates new x and y values.
     *  This may yield duplicate points.
     */
    public Point2D() {
        generateCoords();
    }

    /**
     * Generate new x and y values but ensure that they are not duplicates of past
     * tests or current candidates.
     * @param pastTests List of past test points
     * @param candidates List of current candidate list points
     */
    public Point2D(ArrayList<Point2D> pastTests, ArrayList<Point2D> candidates) {
        boolean sameAsOther;
        do {
            sameAsOther = false;
            generateCoords();

            for (Point2D t: pastTests) {
                if (t.x == this.x && t.y == this.y) {
                    sameAsOther = true;
                    break;
                }
            }
            for (Point2D c: candidates) {
                if (c.x == this.x && c.y == this.y) {
                    sameAsOther = true;
                    break;
                }
            }

        } while (sameAsOther);
    }

    /**
     * Generate new random x and y values in the correct range.
     */
    private void generateCoords() {
        this.x = ((Main.generator.nextDouble() * Main.X_RANGE) + Main.X_RANGE_MIN);
        this.y = ((Main.generator.nextDouble() * Main.Y_RANGE) + Main.Y_RANGE_MIN);
    }
}