package au.net.danielparker.Programs;

import au.net.danielparker.Main;

/**
 * @author Daniel Parker <a href="mailto:dparker.tech@gmail.com">dparker.tech@gmail.com</a>
 */
public class Original {
    public static Main.Sector main (double x, double y) throws ArgumentOutOfRangeException {
        if (Main.X_RANGE_MIN <= x && x < 0.5 * Main.X_RANGE_MAX){
            if (Main.Y_RANGE_MIN <= y && y < 0.5 * Main.Y_RANGE_MAX)
                return Main.Sector.A;
            if (0.5 * Main.Y_RANGE_MAX <= y && y < 0.95 * Main.Y_RANGE_MAX)
                return Main.Sector.B;
            if (0.95 * Main.Y_RANGE_MAX <= y && y <= Main.Y_RANGE_MAX)
                return Main.Sector.C;
        }
        if (0.5 * Main.X_RANGE_MAX <= x && x <= Main.X_RANGE_MAX){
            if (Main.Y_RANGE_MIN <= y && y < 0.5 * Main.Y_RANGE_MAX)
                return Main.Sector.D;
            if (0.5 * Main.Y_RANGE_MAX <= y && y < 0.95 * Main.Y_RANGE_MAX)
                return Main.Sector.E;
            if (0.95 * Main.Y_RANGE_MAX <= y && y <= Main.Y_RANGE_MAX)
                return Main.Sector.F;
        }
        throw new ArgumentOutOfRangeException();
    }
}
