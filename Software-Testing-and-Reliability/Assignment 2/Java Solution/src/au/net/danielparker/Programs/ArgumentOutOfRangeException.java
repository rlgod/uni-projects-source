package au.net.danielparker.Programs;

/**
 * @author Daniel Parker <a href="mailto:dparker.tech@gmail.com">dparker.tech@gmail.com</a>
*/

public class ArgumentOutOfRangeException extends Exception {
    public ArgumentOutOfRangeException() {
        super();
    }
}