package au.net.danielparker;

import au.com.bytecode.opencsv.CSVWriter;
import au.net.danielparker.Programs.ArgumentOutOfRangeException;
import au.net.danielparker.Programs.Faulty1;
import au.net.danielparker.Programs.Original;
import com.sun.xml.internal.fastinfoset.util.StringArray;


import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class Main {
    public static enum Sector { A, B, C, D, E, F }

    public static final int X_RANGE              = 10;
    public static final int Y_RANGE              = 10;
    public static final int X_RANGE_MIN          = 0;
    public static final int X_RANGE_MAX          = X_RANGE_MIN + X_RANGE;
    public static final int Y_RANGE_MIN          = 0;
    public static final int Y_RANGE_MAX          = Y_RANGE_MIN + Y_RANGE;
    public static final int X_FAIL_RANGE_LOWER   = 0;
    public static final int X_FAIL_RANGE_HIGHER  = 10;
    public static final int Y_FAIL_RANGE_LOWER   = 0;
    public static final int Y_FAIL_RANGE_HIGHER  = 10;

    // Number of test iterations to run
    public static final int ITERATIONS           = 2000;

    // Number of candidate points to generate each round.
    public static final int FIXED_CANDIDATE_SIZE = 10;

    // Seed for random number generator.
    public static final int SEED = 10000;

    // Uncomment line below for random seed alternative
    // static int SEED = new Random().nextInt();

    // Global random generator, initialised with SEED
    public static Random generator = new Random(SEED);

    /**
     * Run FSCS-ART random testing, then normal Random Testing
     * @param args Optional csv file path to write summary data to.
     */
    public static void main(String[] args) {
        boolean csvDumpMode = false;
        CSVWriter csvWriter = null;

        // If there was an argument then open it as a file for csv writing.
        if (args.length == 1) {
            try {
                csvWriter = new CSVWriter(new FileWriter(args[0]));
                csvDumpMode = true;
            } catch (IOException e) {
                System.err.println("Failed to open file " + args[0]);
                System.exit(1);
            }
        }

        System.out.println("Failure ratio is " +
                                            ((double)((X_FAIL_RANGE_HIGHER - X_FAIL_RANGE_LOWER) *
                                            (Y_FAIL_RANGE_HIGHER - Y_FAIL_RANGE_LOWER)) / (X_RANGE * Y_RANGE)));

        System.out.println("------------------------------------------------------------------------------\n" +
                "Beginning FSCS-ART testing.");
        Summary fscsartSummary = FSCSART();
        System.out.println("------------------------------------------------------------------------------\n" +
                "On average the FSCS-ART testing approach will take: " + fscsartSummary.averageTime() +
                "ms to find a failure input." +
                "\n------------------------------------------------------------------------------\n");

        System.out.println("\n------------------------------------------------------------------------------\n" +
                "Beginning Random testing.");

        // Re-seed the random number generator to be able to
        // accurately compare FSCS-ART and normal random testing.
        generator.setSeed(SEED);

        Summary randomSummary = RT();
        System.out.println("------------------------------------------------------------------------------\n" +
                "On average the Random testing approach will take: " + randomSummary.averageTime() +
                "ms to find a failure input." +
                "\n------------------------------------------------------------------------------\n");

        System.out.println("------------------------------------------------------------------------------\n" +
                "SUMMARY:\n" +
                "   RANDOM GENERATOR SEED: " + SEED + "\n" +
                "       - FSCS-ART\n" +
                "           Experimental F-Measure: " + fscsartSummary.fMeasure() + "\n" +
                "           Average time to fail:   " + fscsartSummary.averageTime() + " ms\n" +
                "       - Random Testing" + "\n" +
                "           Experimental F-Measure: " + randomSummary.fMeasure() + "\n" +
                "           Average time to fail:   " + randomSummary.averageTime() + " ms\n" +
                "------------------------------------------------------------------------------");

        if (csvDumpMode) {
            fscsartSummary.writeFCounts(csvWriter);
            fscsartSummary.writeTestTimes(csvWriter);

            randomSummary.writeFCounts(csvWriter);
            randomSummary.writeTestTimes(csvWriter);

            try {
                csvWriter.flush();
                csvWriter.close();
            } catch (IOException e){
                System.err.println("Error writing to csv");
                System.exit(2);
            }
        }
    }

    /**
     * Execute and collect data from a set of FSCS-ART testing.
     * @return Test summmary data.
     */
    public static Summary FSCSART() {
        ArrayList<Integer> fCounts = new ArrayList<Integer>();
        ArrayList<Long> testTimes = new ArrayList<Long>();

        for (int i = 0; i < ITERATIONS; i++) {
            long startTime = 0;
            int testsRun = 0;
            try {
                ArrayList<Point2D> pastTests = new ArrayList<Point2D>();
                // Get the start time for the test
                startTime = Calendar.getInstance().getTimeInMillis();

                while (true) {
                    testsRun++;
                    runFSCSARTTest(pastTests);
                }
            } catch (TestFailedException e) {
                long endTime = Calendar.getInstance().getTimeInMillis();
                System.out.print("\r" + ((double)i/(double)ITERATIONS) * 100 + "%");
                fCounts.add(new Integer(testsRun));
                testTimes.add(endTime - startTime);
            } catch (ArgumentOutOfRangeException e) {
                System.err.println("X,Y test program input was out of range");
                System.exit(1);
            }
        }

        Summary thisSummary = new Summary(testTimes, fCounts);

        System.out.println("Experimental FSCS-ART F-Measure: " + thisSummary.fMeasure());

        return thisSummary;
    }


    /**
     * Execute and collect data from a set of random testing
     * @return Test summary data.
     */
    public static Summary RT() {
        ArrayList<Integer> fCounts = new ArrayList<Integer>();
        ArrayList<Long> testTimes = new ArrayList<Long>();

        for (int i = 0; i < ITERATIONS; i++) {
            long startTime = 0;
            int testsRun = 0;
            try {
                // Get the start time for the test
                startTime = Calendar.getInstance().getTimeInMillis();

                while (true) {
                    testsRun++;
                    runRandomTest();
                }
            } catch (TestFailedException e) {
                long endTime = Calendar.getInstance().getTimeInMillis();
                System.out.print(".");
                fCounts.add(new Integer(testsRun));
                testTimes.add(endTime - startTime);
            } catch (ArgumentOutOfRangeException e) {
                System.err.println("X,Y test program input was out of range");
                System.exit(1);
            }
        }

        Summary thisSummary = new Summary(testTimes, fCounts);

        System.out.println("Experimental Random Testing F-Measure: " + thisSummary.fMeasure());

        return thisSummary;
    }

    /**
     * Run a FSCS-ART test
     * @param pastTests
     * @throws TestFailedException
     */
    public static void runFSCSARTTest(ArrayList<Point2D> pastTests) throws TestFailedException, ArgumentOutOfRangeException {
        ArrayList<Point2D> candidates = new ArrayList<Point2D>();

        generateTests(pastTests, candidates);
        Point2D selectedTest = selectTest(pastTests, candidates);

        if (Original.main(selectedTest.x, selectedTest.y) != Faulty1.main(selectedTest.x, selectedTest.y)) {
            throw new TestFailedException("Test failed with input x: " + selectedTest.x + ", y: " + selectedTest.y);
        }
    }

    /**
     * Run a random test
     * @throws TestFailedException
     */
    public static void runRandomTest() throws TestFailedException, ArgumentOutOfRangeException{
        Point2D randomTestInput = new Point2D();

        if (Original.main(randomTestInput.x, randomTestInput.y) != Faulty1.main(randomTestInput.x, randomTestInput.y)) {
            throw new TestFailedException(  "Test failed with input " +
                                            "x: " + randomTestInput.x +
                                            ", y: " + randomTestInput.y);
        }
    }
    /**
     * Generate a set of unique candidate points of size FIXED_CANDIDATE_SET.
     * @param pastTests
     * @param candidates
     */
    public static void generateTests(ArrayList<Point2D> pastTests, ArrayList<Point2D> candidates) {
        for (int i = 0; i < FIXED_CANDIDATE_SIZE; i++){
            candidates.add(new Point2D(pastTests, candidates));
        }
    }

    /**
     * Select a test from the candidate set that is furthest away
     * from past executed tests in the 2D plane.
     * @param pastTests
     * @param candidates
     * @return The selected Point2D
     */
    public static Point2D selectTest(ArrayList<Point2D> pastTests, ArrayList<Point2D> candidates) {
        double bestDist = -1;
        Point2D selectedPoint = null;

        for( Point2D c: candidates) {
            double min_distance = Double.MAX_VALUE;

            for ( Point2D t: pastTests ) {
                min_distance =  Math.min(min_distance,
                                Math.sqrt(Math.pow((c.x - t.x), 2.0) + Math.pow((c.y - t.y), 2.0)));
            }

            if (bestDist < min_distance) {
                selectedPoint = c;
                bestDist = min_distance;
            }
        }

        pastTests.add(selectedPoint);
        return selectedPoint;
    }

    /**
     * A simulated domain of x,y inputs that fails in 0.001 cases.
     * @param x Test x value
     * @param y Test y value
     * @return false for fail true for pass
     * @throws IllegalArgumentException when x and y are out of range
     */
    public static boolean testDomain(double x, double y) throws IllegalArgumentException{
        if (!(x >= X_RANGE_MIN && y >= Y_RANGE_MIN && x <= X_RANGE_MAX && y <= Y_RANGE_MAX)) {
            System.err.println("Input values were out of range. Please provide numbers between " +
                                X_RANGE_MIN + " and " + X_RANGE_MAX);
            throw new IllegalArgumentException();
        }

        if (    x >= X_FAIL_RANGE_LOWER && x <= X_FAIL_RANGE_HIGHER
                && y >= Y_FAIL_RANGE_LOWER && y <= Y_FAIL_RANGE_HIGHER ) {
            return false;
        } else
            return true;
    }

    /**
     *  Point2D model. A point has type double x and y coordinates.
     */


    /**
     * Class that contains a test summary
     */
    static class Summary {
        public ArrayList<Long> testTimes = new ArrayList<Long>();
        public ArrayList<Integer> fCounts = new ArrayList<Integer>();

        public Summary( ArrayList<Long> testTimes, ArrayList<Integer> fCounts ) {
            this.testTimes = testTimes;
            this.fCounts = fCounts;
        }

        public double fMeasure() {
            int fCountSum = 0;
            for (int f: fCounts) {
                fCountSum += f;
            }

            return fCountSum / ITERATIONS;
        }

        public double averageTime() {
            long testTimeSums = 0;
            for (long l: testTimes) {
                testTimeSums += l;
            }

            return testTimeSums / ITERATIONS;
        }

        public void writeFCounts(CSVWriter csvWriter) {
            StringArray stringArray = new StringArray();
            stringArray.add("FCount");
            for (Integer i: fCounts) {
                stringArray.add(i.toString());
            }

            csvWriter.writeNext(stringArray.getArray());
        }

        public void writeTestTimes(CSVWriter csvWriter) {
            StringArray stringArray = new StringArray();
            stringArray.add("Times");
            for (Long l: testTimes) {
                stringArray.add(l.toString());
            }

            csvWriter.writeNext(stringArray.getArray());
        }
    }

    /**
     * Custom exception for test failure
     */
    static class TestFailedException extends Exception {
        public TestFailedException() {

        }

        public TestFailedException(String message) {
            super(message);
        }
    }
}
