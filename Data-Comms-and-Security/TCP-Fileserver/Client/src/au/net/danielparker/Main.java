/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import org.apache.commons.cli.*;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Main class, entry point, initialiser and command line argument parsing
 * @author Daniel Parker 971328X
 */
public class Main {
    public static final String PROGRAM_NAME = "TCP File Client";                            // Name of program
    public static final int EXIT_FAILURE = 1;                                               // Status when program fails
    public static final int PORT_MIN = 1;                                                   // Minimum port value
    public static final int PORT_MAX = 65535;                                               // Maximum port value
    public static final String ARG_PORT = "p";                                              // Port Argument
    public static final String ARG_HELP = "h";                                              // Help Argument
    public static final String ARG_IP = "i";                                                // IP Argument
    public static final String ARG_FILENAME = "f";                                          // Filename Argument

    /**
     * Program entry point, initializer, command parsing
     * @param args  Execution arguments
     */
    public static void main(String[] args) {
        Options options = new Options(); // Creates options object
        // Create command line options
        Option serverIP       = OptionBuilder.withArgName("ip")
                                           .hasArg()
                                           .withDescription("server IP address to connect to")
                                           .create(ARG_IP);
        Option serverPort   = OptionBuilder.withArgName("port")
                                           .hasArg()
                                           .withDescription("server port")
                                           .create(ARG_PORT);
        Option fileName     = OptionBuilder.withArgName("filename")
                                           .hasArg()
                                           .withDescription("File to retrieve from server")
                                           .create(ARG_FILENAME);
        Option help         = OptionBuilder.withDescription("view this help message")
                                           .create(ARG_HELP);

        // Add all command line options
        options.addOption(serverIP)
               .addOption(serverPort)
               .addOption(fileName)
               .addOption(help);

        CommandLineParser commandLineParser = new BasicParser();                            // Create a basic command line parser
        HelpFormatter helpFormatter = new HelpFormatter();                                  // Create help formatter
        try {
            CommandLine cmd = commandLineParser.parse(options, args);                       // Parse the command line options
            if (cmd.hasOption(ARG_HELP)) {                                                  // Check for '-h' argument
                helpFormatter.printHelp(PROGRAM_NAME, options);                             // Print help message
            }
            else {
                if (cmd.hasOption(ARG_IP) && cmd.hasOption(ARG_PORT) && cmd.hasOption(ARG_FILENAME)) {

                    String filename = cmd.getOptionValue(ARG_FILENAME);                     // Get file name
                    InetAddress ipAddress = InetAddress.getByName(cmd.getOptionValue(ARG_IP)); // Get ip address from cmd parser

                    int port = Integer.parseInt(cmd.getOptionValue(ARG_PORT));              // Get port from cmd parser
                    if (!(port >= PORT_MIN && port <= PORT_MAX)) {                          // Validate port
                        System.out.println("ERROR: Port must be within the range " + PORT_MIN + " - " + PORT_MAX);
                        System.exit(EXIT_FAILURE);                                          // Exit program with 1
                    }

                    System.out.println("Running client");
                    TCPFileClient tcpFileClient = new TCPFileClient(ipAddress, port);       // Create new client object
                    tcpFileClient.getFile(filename);                                        // Start the client
                } else {
                    helpFormatter.printHelp(PROGRAM_NAME, options);                         // Print help message
                    System.exit(EXIT_FAILURE);                                              // Exit with failure code
                }
            }

        } catch (NumberFormatException e) {
            System.err.println("Number was too large to store in an integer");              // Occurs when a number exceeding the maximum or minimum allowed is entered for the port
        }
        catch (ParseException e) {
            System.err.println("Error parsing commandline");
        } catch (UnknownHostException e) {
            System.err.println("Unknown host");
        }
    }
}
