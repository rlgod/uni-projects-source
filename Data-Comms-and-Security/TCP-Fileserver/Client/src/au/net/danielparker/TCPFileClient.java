/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import java.io.*;
import java.net.*;

/**
 * The local file client which request and receives a file from a server
 * @author Daniel Parker 971328X
 */
public class TCPFileClient {
    private InetAddress serverAddress;                                                  // Server address
    private static final int BUFFER_SIZE = 1024;                                        // Buffer size
    private static final int DEFAULT_OFFSET = 0;                                        // Byte array offset
    private static final int COMMAND_LENGTH = 2;                                        // Command length
    private static final int END_OF_STREAM = -1;                                        // InputStream end of stream value
    private int serverPort;

    /**
     * Protected default constructor to stop usage of it over the overloaded constructor
     */
    protected TCPFileClient() {}

    /**
     * Public constructor
     * @param serverAddress IP address of the file server
     * @param serverPort    Port that the server is listening on
     */
    public TCPFileClient(InetAddress serverAddress, int serverPort) {
        this.serverAddress = serverAddress;         // Set server address
        this.serverPort = serverPort;               // Set server port
    }

    /**
     * Main method that sends request to server and retrieves the file
     * @param filename  The name of the file on the server that is requested
     */
    public void getFile(String filename) {
        try {
            String command = "send " + filename;                                        // Construct command to send server

                Socket socket = new Socket(serverAddress, serverPort);                  // Create TCP socket to server
                InputStream inStream = socket.getInputStream();                         // Get an instance of the socket's input stream
                PrintWriter output = new PrintWriter(socket.getOutputStream(), true);   // Create a PrintWriter for sending commands

                output.println(command);                                                // Send command as line
                byte[] received = new byte[COMMAND_LENGTH];                             // Buffer for receiving the response command
                inStream.read(received);                                                // Read the 2 byte command
                inStream.read();                                                        // Read and throw away the new line
                String receivedString = new String(received);                           // Convert command to string

                if (receivedString.equals("/F")){                                       // Check if the server will send the file

                    System.out.println("Receiving file...");

                    File outFile = new File(filename);                                  // Create File object where the received file will go
                    outFile.createNewFile();                                            // Create that file on the drive

                    OutputStream fileOutputStream = new FileOutputStream(outFile);      // Create OutputStream object to the file

                    byte[] data = new byte[BUFFER_SIZE];                                // data buffer for receiving file
                    int bytesRead;                                                      // Number of bytes received

                    while ((bytesRead = inStream.read(data)) != END_OF_STREAM) {        // Loop and read data while stream isn't ended
                        fileOutputStream.write(data, DEFAULT_OFFSET, bytesRead);        // Write the read data to file
                    }
                    fileOutputStream.close();

                    System.out.println("Finished receiving file");

                } else if (receivedString.equals("/E")) {                               // Check if the server is sending an error
                    System.out.println(ReadLine(inStream));                             // Read and print the error
            }
        } catch (ConnectException e){
            System.err.println("Connection refused");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Reads a line of data from the given stream and converts it to a String
     * @param inStream  The InputStream to read line from
     * @return  A string of the received line
     */
    public static String ReadLine(InputStream inStream) {
        int bytesRead;                                                                  // Count number of bytes received
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();      // Where the data will be dumped when it's read
        byte[] data = new byte[BUFFER_SIZE];                                            // Temporary buffer
        try {
            while ((bytesRead = inStream.read(data)) != END_OF_STREAM) {                // Loop and read while not end of stream
                byteArrayOutputStream.write(data, DEFAULT_OFFSET, bytesRead);           // Dump this buffer into the byteArrayOutputStream
            }
            return byteArrayOutputStream.toString();                                    // Convert the dumped data to a String and return it
        } catch (IOException e) {
            System.err.println("Error reading from network");
            return "Problem reading error message";                                     // Returned string if there was an error in this method
        }
    }
}
