/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import org.apache.commons.cli.*;

/**
 * TCP File Server Main class
 * @author Daniel Parker 971328X
 */
public class Main {
    public static final String PROGRAM_NAME = "TCP File Server";                        // Program name
    public static final int EXIT_FAILURE = 1;                                           // Failure status code
    public static final int PORT_MIN = 1;                                               // Minimum port number
    public static final int PORT_MAX = 65535;                                           // Maximum port number
    public static final String ARG_PORT = "p";                                          // Port Argument
    public static final String ARG_HELP = "h";                                          // Help Argument

    /**
     * Program entry point. Initializes object instances and parses command line input
     * @param args String array of execution arguments
     */
    public static void main(String[] args) {
        Options options = new Options();                                                // Creates Options object

        // Create command line options
        Option serverPort   = OptionBuilder.withArgName("port")                         // Argument Name
                .hasArg()                                                               // Requires an argument value as well
                .withDescription("Port to connect to")                                  // Description for help message
                .create(ARG_PORT);                                                      // Short argument
        Option help         = OptionBuilder.withDescription("view this help message")   // Description for help message
                .create(ARG_HELP);                                                      // Short argument

        // Add all command line options
        options.addOption(serverPort)
                .addOption(help);

        CommandLineParser commandLineParser = new BasicParser();                        // Create a basic parser
        HelpFormatter helpFormatter = new HelpFormatter();                              // Create a help formatter

        try {
            CommandLine cmd = commandLineParser.parse(options, args);                   // Parse command line arguments
            if (cmd.hasOption(ARG_HELP)) {                                              // If '-h' entered
                helpFormatter.printHelp(PROGRAM_NAME, options);                         // Print help message
            }
            else {
                if (cmd.hasOption(ARG_PORT)) {                                          // If '-p' found
                    int port = Integer.parseInt(cmd.getOptionValue(ARG_PORT));          // Parse port to int
                if (!(port >= PORT_MIN && port <= PORT_MAX)) {                          // Validate port
                        System.out.println("ERROR: Port must be within the range " + PORT_MIN + " - " + PORT_MAX);
                        System.exit(EXIT_FAILURE);                                      // Exit program with 1
                    }
                    TCPFileServer tcpFileServer = new TCPFileServer();                  // Create a new TCPFileServer
                    System.out.println("Running file server...");                       // Server start user feedback
                    tcpFileServer.start(port);                                          // Start the server with port
                } else {
                    helpFormatter.printHelp(PROGRAM_NAME, options);                     // Print help message
                    System.exit(EXIT_FAILURE);                                          // Exit program with status 1
                }
            }

        } catch (NumberFormatException e) {
            System.err.println("Number was too large to store in an integer");
        }
        catch (ParseException e) {
            System.err.println("Error parsing commands");
        }
    }
}
