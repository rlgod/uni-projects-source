/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * File Server main listener
 * @author Daniel Parker 971328X
 */
public class TCPFileServer {

    /**
     * Starts the main loop which listens for connections and spawns threads for them.
     * @param port  Network port to bind server to.
     */
    public void start(int port) {
        ServerSocket serverSocket = null;
        try {
            serverSocket = new ServerSocket(port);                          // Create and bind a ServerSocket to port
            while (true) {                                                  // Loop forever
                try {
                    Socket newConnection = serverSocket.accept();           // Accepts a new connection (blocks until then)
                    Thread clientThread = new Thread(new ClientConnectionHandler(newConnection));   // Create a thread for the connection
                    System.out.println("Client Connected");
                    clientThread.start();                                   // Start thread
                } catch (IOException e) {
                    System.err.println("Error accepting connection");
                }
            }
        } catch (IOException e) {
            System.err.println("Error creating server socket");
        } finally {
            if (serverSocket != null) {                                     // If there was a server socket
                try {
                    serverSocket.close();                                   // Try to close it
                } catch (IOException e) {
                    System.err.println("Error closing server socket");
                }
            }
        }
    }
}
