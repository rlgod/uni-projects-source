/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import java.io.*;
import java.net.Socket;
import java.nio.file.FileSystems;
import java.nio.file.Path;

/**
 * A runnable that can be used as a thread parameter to handle a single client connection
 * @author Daniel Parker 971328X
 */
public class ClientConnectionHandler implements Runnable {
    private Socket sock;        // Socket for this client TCP connection
    private static final int COMMAND_PARTS = 2;                                                         // Number of expected parts in a command
    private static final int BUFFER_SIZE = 1024;                                                        // Buffer size for reading from file and sending over network

    /**
     * Protected default constructor to stop it from being used externally
     * The overloaded ClientConnectionHandler(Socket) should be used instead
     */
    protected ClientConnectionHandler(){}

    /**
     * Public constructor
     * @param sock  Socket that the client is connected on
     */
    public ClientConnectionHandler(Socket sock) {
        this.sock = sock;                                                                               // Set sock field to given socket
    }

    /**
     * Run method needed to implement Runnable interface. Simply calls clientConnection method with socket
     */
    public void run() {
        clientConnection();
    }

    /**
     * Method which handles the rest of the client connection and request.
     * Validates request and serves file to client.
     */
    public void clientConnection() {
        try {
            System.setProperty("line.separator", "\n");
            BufferedReader inStream = new BufferedReader(new InputStreamReader(sock.getInputStream())); // Get a buffered reader from the socket input stream
            OutputStream outStream = sock.getOutputStream();                                            // Get an instance of the socket's output stream
            PrintWriter outputWriter = new PrintWriter(outStream, true);                                // Create a separate print writer for the same output stream
            String command;                                                                             // string to store read command

            if ((command = inStream.readLine()) != null) {                                              // Read a single line from the network socket and check if not null

                System.out.println(command);                                                            // Print out the command that was received on the local terminal

                String[] commandParts = command.split("\\s+");                                          // Split the command wherever there's whitespace including space, tab, new line and carriage return
                if (commandParts.length == COMMAND_PARTS) {                                             // Check if the command has the correct number of parts
                    commandParts[0] = commandParts[0].toUpperCase();                                    // Convert first part to upper case
                    if (commandParts[0].equals("SEND")) {                                               // Check if 'SEND' was the first part

                        Path pathHere = FileSystems.getDefault().getPath(commandParts[1]);              // Create a Path object for the filename requested
                        File fileToSend = pathHere.toFile();                                            // Retrieve File object from that path

                        if (fileToSend.exists()) {                                                      // Check that the file exists
                            System.out.println(fileToSend.getAbsolutePath());                           // Print the path of the file that will be sent
                            BufferedInputStream fileInputStream = new BufferedInputStream( new FileInputStream(fileToSend));          // Create an input stream for the file
                            byte[] byteData = new byte[BUFFER_SIZE];                                    // Create a buffer to temporarily store read data before sending
                            int dataRead;                                                               // Variable to store the size of data read in bytes
                            outputWriter.println("/F");                                         // Send command to client so they expect a file
                            outStream = sock.getOutputStream();
                            while ((dataRead = fileInputStream.read(byteData)) != -1) {                 // Loop until the file input stream closes and read into the buffer
                                outStream.write(byteData, 0, dataRead);                                 // Send data to client using output stream
                            }
                            outStream.close();                                                          // Close the stream to ensure a -1 is sent
                        } else {
                            outputWriter.println("/E");                                                 // Tell client to expect an error message
                            outputWriter.println("No file of that name.");                              // Send that the file was not found
                        }
                    } else {
                        outputWriter.println("/E");                                                     // Tell client to expect an error message
                        outputWriter.println(("Unknown command: " + commandParts[0]));                  // Send that the command was unknown
                    }
                } else {
                    outputWriter.println("/E");                                                         // Tell client to expect an error message
                    outputWriter.println("Not enough arguments");                                       // Send that there weren't enough arguments in the command
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("File not found");
        } catch (IOException e){
            System.err.println("IO Error");
        }
    }
}