/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import java.io.IOException;
import java.net.*;

/**
 * Main UDP Server class. Runs in a loop
 * @author Daniel Parker 971328X
 */
public class UDPServer {
    private static final int BUFFER_SIZE = 1024;
    private DatagramSocket socket;

    public void run(int port) {
        try {
            socket = new DatagramSocket(port);                  // New UDP Socket
            String message;                                     // Where the return message will be stored
            byte[] receivedData;                                // Temporary receive buffer
            byte[] sendData;                                    // Temporary send buffer

            System.out.println("Listening...");
            while (true) {                                                                  // Loop to continue server running
                try {
                    receivedData = new byte[BUFFER_SIZE];                                   // New Buffer
                    DatagramPacket receivedPacket = new DatagramPacket(receivedData, receivedData.length);  // Packet to receive to
                    socket.receive(receivedPacket);                                         // Receive data from socket to packet
                    String command = new String(receivedPacket.getData());                  // Convert received data to string
                    System.out.println("RECEIVED: " + command.trim());

                    if (Utilities.processAndCompareCommand(command, "Hello Server")) {      // Check if the command 'Hello Server' was received
                        message = "Hello, my name is Daniel and my Id is 971328X";          // Message to respond with on success
                    } else {
                        message = "NO";                                                     // Response if command was not recognised
                    }

                    InetAddress IPAddress = receivedPacket.getAddress();                    // Retrieve IP address from received packet to know where to response to
                    int clientPort = receivedPacket.getPort();                              // Retrieve Port from received packet to know response port
                    sendData = message.getBytes();                                          // Convert response message to byte array
                    DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, IPAddress, clientPort);   // Prepare response UDP Packet
                    socket.send(sendPacket);                                                // Send UDP packet
                } catch (IOException e) {
                    System.out.println("Problem occurred when sending or receiving data");
                }
            }
        } catch (SocketException e) {
            System.err.println("Problem occurred when opening socket");
        }
    }
}
