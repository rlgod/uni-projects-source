/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import org.apache.commons.cli.*;

public class Main {
    public static final String PROGRAM_NAME = "Hello UDP Server";                   // Used for printing help
    public static final int PORT_MIN = 1;                                           // Minimum port number
    public static final int PORT_MAX = 65535;                                       // Maximum port number
    public static final int RETURN_ERROR = 1;                                       // Failure exit code
    public static final String ARG_PORT = "p";                                      // Argument for port
    public static final String ARG_HELP = "h";                                      // Argument for help

    public static void main(String[] args) {
        Options options = new Options(); // Creates Options object

        /**
         * Create command line options
         */
        Option serverPort   = OptionBuilder.withArgName("port")
                .hasArg()
                .withDescription("Port to connect to")
                .create(ARG_PORT);
        Option help         = OptionBuilder.withDescription("view this help message")
                .create(ARG_HELP);

        /**
         * Add all command line options
         */
        options.addOption(serverPort)
                .addOption(help);

        CommandLineParser commandLineParser = new BasicParser();                    // Create a parser
        HelpFormatter helpFormatter = new HelpFormatter();                          // Create a help formatter

        try {
            CommandLine cmd = commandLineParser.parse(options, args);               // Parse input to CommandLine Object
            if (cmd.hasOption(ARG_HELP)) {                                          // Check if '-h' was entered
                helpFormatter.printHelp(PROGRAM_NAME, options);                     // Print command help message
            }
            else {
                if (cmd.hasOption(ARG_PORT)) {                                      // Check if a port was given
                    int port = Integer.parseInt(cmd.getOptionValue(ARG_PORT));      // Get port from cmd parser
                    if (!(port >= PORT_MIN && port <= PORT_MAX)) {                  // Validate port
                        System.out.println("ERROR: Port must be within the range " + PORT_MIN + " - " + PORT_MAX);
                        System.exit(RETURN_ERROR);                                  // Exit program with 1
                    }

                    UDPServer server = new UDPServer();                             // Create server object
                    server.run(port);                                               // Start the server
                } else {
                    helpFormatter.printHelp(PROGRAM_NAME, options);                 // Print command help message
                    System.exit(RETURN_ERROR);
                }
            }

        } catch (ParseException e) {
            System.err.println("Error parsing commands");
        } catch (NumberFormatException e) {
            System.err.println("There was a problem parsing the given port");
        }
    }
}