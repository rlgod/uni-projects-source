/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

/**
 * Utility methods used frequently.
 * @author Daniel Parker 971328X
 */
public class Utilities {
    /**
     * Compare given command against a test string
     * @param command   Data to test
     * @param test  String to compare to
     * @return  True if they're the same otherwise false
     */
    public static boolean processAndCompareCommand(String command, String test) {
        String trimmedString = command.trim();          // Remove whitespace
        trimmedString = trimmedString.toUpperCase();    // Convert to upper case
        test = test.toUpperCase();                      // Convert test string to upper case
        if (trimmedString.equals(test)) {               // Test for equality
            return true;
        }
        else {
            return false;
        }
    }
}
