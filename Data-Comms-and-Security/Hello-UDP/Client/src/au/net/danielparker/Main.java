/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import org.apache.commons.cli.*;
import org.apache.commons.validator.routines.InetAddressValidator;

import java.net.*;

/**
 * Main class, entry point, initialiser, command parser
 * @author Daniel Parker 971328X
 */
public class Main {
    public static final String PROGRAM_NAME = "Hello UDP Client";   // Used for printing help
    public static final int PORT_MIN = 1;                           // Minimum port number
    public static final int PORT_MAX = 65535;                       // Maximum port number
    public static final int RETURN_ERROR = 1;                       // Failure exit code
    public static final String ARG_IP = "i";                        // Argument for ip
    public static final String ARG_PORT = "p";                      // Argument for port
    public static final String ARG_HELP = "h";                      // Argument for help

    /**
     * Main class
     * @param args execute arguments
     */
    public static void main(String[] args) {
        Options options = new Options(); // Creates options object

        /**
         * Create command line options
         */
        Option serverIP     = OptionBuilder.withArgName("ip")
                .hasArg()
                .withDescription("IP address to connect to")
                .create(ARG_IP);
        Option serverPort   = OptionBuilder.withArgName("port")
                .hasArg()
                .withDescription("Port to connect to")
                .create(ARG_PORT);
        Option help         = OptionBuilder.withDescription("view this help message")
                .create(ARG_HELP);

        /**
         * Add all command line options
         */
        options.addOption(serverIP)
                .addOption(serverPort)
                .addOption(help);

        HelpFormatter helpFormatter = new HelpFormatter();              // Create help Formatter
        CommandLineParser commandLineParser = new BasicParser();        // Create a basic parser instance

        try {
            CommandLine cmd = commandLineParser.parse(options, args);   // Parse to Commandline Object
            if (cmd.hasOption(ARG_HELP)) {                              // Check if '-h' was entered
                helpFormatter.printHelp(PROGRAM_NAME, options);         // Print help message
            }
            else {
                if (cmd.hasOption(ARG_IP) && cmd.hasOption(ARG_PORT)) {

                    InetAddressValidator IPValidator = InetAddressValidator.getInstance();  // Get instance of InetAddressValidator Singleton
                    InetAddress address = null;
                    int port;

                    if (IPValidator.isValidInet4Address(cmd.getOptionValue(ARG_IP))) {      // Validate IP
                        address = InetAddress.getByName(cmd.getOptionValue(ARG_IP));        // Get ip address from cmd parser
                    } else {
                        System.out.println("Invalid IPv4 address");
                        System.exit(RETURN_ERROR);
                    }

                    port = Integer.parseInt(cmd.getOptionValue(ARG_PORT));                  // Get port from cmd parser
                    if (!(port >= PORT_MIN && port <= PORT_MAX)) {                          // Validate port
                        System.out.println("ERROR: Port must be within the range " + PORT_MIN + " - " + PORT_MAX);
                        System.exit(RETURN_ERROR);                                          // Exit program with 1
                    }
                    UDPClient client = new UDPClient();                                     // Create new UDPClient Object
                    client.run(address, port);                                              // Start the client
                } else {
                    helpFormatter.printHelp(PROGRAM_NAME, options);                         // Print help message
                    System.exit(RETURN_ERROR);                                              // Exit with status 1
                }
            }

        } catch (ParseException e) {
            System.err.println("Error parsing commands");
        } catch (UnknownHostException e) {
            System.err.println("Unknown host");
        } catch (NumberFormatException e) {
            System.err.println("There was a problem parsing the given port");
        }
    }
}
