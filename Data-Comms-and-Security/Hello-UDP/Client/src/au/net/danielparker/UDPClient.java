/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import java.io.IOException;
import java.net.*;

/**
 * UDP Client sends and receives data from server
 * @author Daniel Parker
 */
public class UDPClient {
    private static final int BUFFER_SIZE = 1024;            // Buffer size
    private DatagramSocket socket;

    /**
     * Receive data from UDP server. Will block until data is available
     */
    private void receiveData() {
        {
            byte[] data = new byte[BUFFER_SIZE];                            // New byte array of buffer size
            DatagramPacket packet = new DatagramPacket(data, data.length);  // A new datagram packet with above byte array buffer
            try {
                socket.receive(packet);                                     // Receive data to datagram packet
            } catch (IOException e) {
                System.err.println("Error receiving data from socket");
            }
            String message = new String(packet.getData());                  // Convert data to string
            if (!Utilities.processAndCompareCommand(message, "NO")) {       // Process response message
                System.out.println(message.trim());                         // Print message to terminal
            }
            else {
                System.out.println("UNKNOWN COMMAND");
            }
        }
    }

    /**
     * Send command to server using UDP
     * @param IPAddress Server IP Address
     * @param port  Server port
     */
    public void run(InetAddress IPAddress, int port) {
        try {
            socket = new DatagramSocket();                              // UDP socket
            String requestString = "Hello Server";                      // Message to send to server
            DatagramPacket dataPacket = new DatagramPacket(requestString.getBytes(), requestString.length(), IPAddress, port); // Prepare UDP Packet
            System.out.println("Sending: '" + requestString + "'");
            socket.send(dataPacket);                                    // Send UDP Packet
            receiveData();                                              // Receive response

        } catch (UnknownHostException e) {
            System.err.println("Unknown Host" + e.getMessage());
        } catch (SocketException e) {
            System.err.println("Datagram socket error");
        } catch (IOException e) {
            System.err.println("Error reading from console");
        }
    }
}
