/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import org.apache.commons.cli.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * Entry point, initializer and command parser
 * @author Daniel Parker 971328X
 */
public class Main {
    final static int MESSAGE_BUFFER_SIZE = 2048;                                        // Buffer size for messages
    final static int EXIT_FAILURE = 1;                                                  // Failure Exit code
    public static final int PORT_MIN = 1;                                               // Minimum port number
    public static final int PORT_MAX = 65535;                                           // Maximum port number
    public static final String ARG_PORT = "p";                                          // Port Argument
    public static final String ARG_FILENAME = "f";                                      // Filename argument
    public static final String ARG_HELP = "h";                                          // Help argument
    public static final String PROGRAM_NAME = "P2P Chat";                               // Program name

    /**
     * Program entry point, command parser, initializer
     * @param args  Execution arguments
     */
    public static void main(String[] args) {
        Options options = new Options(); // Creates options object
        // Create command line options

        Option serverPort   = OptionBuilder.withArgName("port")
                .hasArg()
                .withDescription("server port")
                .create(ARG_PORT);
        Option fileName     = OptionBuilder.withArgName("peers filename")
                .hasArg()
                .withDescription("File containing peers. One peer per line with the format 'Name, IP'")
                .create(ARG_FILENAME);
        Option help         = OptionBuilder.withDescription("view this help message")
                .create(ARG_HELP);

        // Add all command line options
        options.addOption(serverPort)
                .addOption(fileName)
                .addOption(help);

        CommandLineParser commandLineParser = new BasicParser();                            // Create new basic parser
        HelpFormatter helpFormatter = new HelpFormatter();                                  // Create a help formatter
        try {
            CommandLine cmd = commandLineParser.parse(options, args);                       // Parse commands
            if (cmd.hasOption(ARG_HELP)) {                                                  // Check for '-h'
                helpFormatter.printHelp(PROGRAM_NAME, options);                             // Print the help message
            }
            else {
                if (cmd.hasOption(ARG_FILENAME) && cmd.hasOption(ARG_PORT)) {               // Check for port and peers filename
                    System.out.println("Initializing...");
                    String filename = cmd.getOptionValue(ARG_FILENAME);                     // Get the peers file name
                    int port = Integer.parseInt(cmd.getOptionValue(ARG_PORT));              // Get port from cmd parser

                    if (!(port >= PORT_MIN && port <= PORT_MAX)) {                          // Validate port
                        System.out.println("ERROR: Port must be within the range " + PORT_MIN + " - " + PORT_MAX);
                        System.exit(EXIT_FAILURE);                                          // Exit program with 1
                    }

                    Peers peers = new Peers(filename, port);                                // Create the peers object
                    final Server server = new Server(peers, port);                          // Create a server object (all clients have one)
                    Thread serverThread = new Thread(new Runnable() {                       // Thread the server listen method
                        @Override
                        public void run() {
                            server.listen();
                        }
                    });
                    serverThread.start();                                                   // Start thread
                    StartChatting(peers);                                                   // Start checking for local messages to send

                } else {
                    helpFormatter.printHelp(PROGRAM_NAME, options);                         // Print help
                    System.exit(EXIT_FAILURE);                                              // Exit with failure code
                }
            }

        } catch (NumberFormatException e) {
            System.err.println("Port number to large to store in an integer");
        }
        catch (ParseException e) {
            System.err.println("Error parsing command arguments");
        }
    }

    /**
     * Start the chat loop, checks for input on this machine and sends if there was
     * @param peers The peers object to use for sending
     */
    public static void StartChatting(Peers peers) {
        byte[] myMessage = new byte[MESSAGE_BUFFER_SIZE];                                       // Buffer for messages
        int bytesRead;                                                                          // Count for bytes

        System.out.println("Type message and press 'return' to send");                          // Initial one-time prompt

        while(true) {                                                                           // Loop forever
            if (System.console() != null) {                                                     // Retrieve system console and check for null
                String message = System.console().readLine();                                   // Read from console to message string
                peers.sendAll(message);                                                         // Send message to all peers
                System.out.println("You <IP 127.0.0.1>: " + message);                           // Print you're message and that you sent it
            } else {                                                                            // If a console object could not be retrieved go back to this
                try {
                    ByteArrayOutputStream messageStream = new ByteArrayOutputStream();          // Create a place to store the message during construction
                    while (System.in.available() > 0) {                                         // Check if there's something to read
                        bytesRead = System.in.read(myMessage);                                  // Read
                        messageStream.write(myMessage, 0, bytesRead);                           // Write to ByteArrayOutputStream
                    }
                    if (messageStream.size() > 0) {                                             // Check if there was anything written
                        peers.sendAll(messageStream.toString());                                // Send it to all the peers
                        System.out.println("You <IP 127.0.0.1>: " + messageStream.toString());  // Print the message and that it was you that sent it
                    }
                } catch (IOException e) {
                    System.err.println("Error reading input");
                }
            }
        }
    }
}
