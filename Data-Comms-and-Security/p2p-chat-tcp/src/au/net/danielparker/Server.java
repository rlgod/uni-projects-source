/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

/**
 * TCP chat server class, spawns new threads for accepted connections
 * @author Daniel Parker 971328X
 */
public class Server {
    private ServerSocket serverSocket;                          // Local server socket
    private Peers peers;                                        // Local peers object
    private static int EXIT_FAILURE = 1;                        // Failure status code

    /**
     * Protected default constructor so that it's not used
     */
    protected Server(){}

    /**
     * Constructor
     * @param peers An initialised peers object
     * @param port  Port to listen on
     */
    public Server(Peers peers, int port) {
        this.peers = peers;                                     // Set local peers file
        try {
            serverSocket = new ServerSocket(port);              // Create ServerSocket on given port
        } catch (IOException e) {
            System.err.println("Could not create server socket");
            System.exit(EXIT_FAILURE);
        }
    }

    /**
     * Listen for connections, spawn threads for new accepted connections
     */
    public void listen() {
        while (true) {
            try {
                final Socket socket = serverSocket.accept();                                // Accept incoming connection and create socket
                if (peers.isValidIP(socket.getInetAddress())) {                             // Check if connection source IP is a valid one
                    if (!peers.isMyAddress(socket.getInetAddress())) {                      // Check if it's my own message
                        Thread connection = new Thread(new Runnable() {                     // Create a new thread for the connection
                            @Override
                            public void run() {
                                try {
                                    while (socket.isConnected()) {                          // Loop while still connected
                                        BufferedReader netInput = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                                        if (netInput.ready()) {                             // Check if there's something to read
                                            String input = netInput.readLine();             // Read a line
                                            String peerName = peers.getName(socket.getInetAddress());   // Get the peer's name
                                            lockedPrintLn(peerName + " <IP " + socket.getInetAddress().getHostAddress() + ">: " + input);   // Print the message
                                        }
                                    }
                                    socket.close();                                         // If the peer closed then we close as well
                                } catch (IOException e) {
                                    System.err.println("Problem reading input");
                                }
                            }
                        });
                        connection.start();                                                 // Start the thread
                    }
                } else if (!peers.unauthAlreadyWarned(socket.getInetAddress())) {           // If the invalid peer IP has not already been printed in a warning
                    lockedPrintLn("Unauthorized chat request from <IP " + socket.getInetAddress().getHostAddress() + ">");  // Print a warning that it's unauthorized
                }
            } catch (SocketException e) {
                System.err.println(e.getMessage());
            } catch (IOException e) {
                System.err.println(e.getMessage());
            }
        }
    }

    /**
     * Lock console before printing (may be redundant)
     * @param s String to print
     */
    public static void lockedPrintLn(String s) {
        synchronized (System.out) {                                 // Concurrency utility, synchronize the output stream to terminal
            System.out.println(s);
        }
    }
}
