/**
 * DISCLAIMER:
 * All code is original and written by me, not sourced from third party websites.
 * The only reference has been the Java SE 7 API reference
 *
 * See: http://docs.oracle.com/javase/7/docs/api/
 *
 */
package au.net.danielparker;

import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Object which reads peers from file and handles all message sending and validation
 * @author Daniel Parker 971328X
 */
public class Peers {
    private final int LOG_SIZE_BYTES = 1048576;             // Default Log file max size
    private final int NUM_LOG_FILES = 1;                    // Maximum number of log files to create
    private int port;
    private final int PEER_PARTS = 2;
    private Hashtable<InetAddress, String> peersByAddress;  // Collection of peer names 'value' by ip address 'key'
    private Hashtable<InetAddress, Socket> socketsByAddress;// Collection of peer sockets 'value' by by ip address 'key'
    private List<InetAddress> peerIPAddresses;              // List of valid IP Addresses
    private List<InetAddress> unauthorisedIPAddresses;      // List of unauthorized IP Addresses
    private List<InetAddress> allMyIPAddresses;             // List of this computer's IP Addresses

    private final static Logger LOGGER = Logger.getLogger(Peers.class.getName()); // New Logger object for this class

    /**
     * Public constructor which loads the peers from the file and instantiates
     * object fields. Has it's own logger.
     *
     * @param peerFileName  File name of comma delimited peers (one per line)
     */
    public Peers(String peerFileName, int port) {
        try {
            LOGGER.setLevel(Level.INFO);                                            // Logging level INFO and above
            LOGGER.setUseParentHandlers(false);                                     // Ensure that parent handlers do not enforce settings
            FileHandler logHandler = new FileHandler("log", LOG_SIZE_BYTES, NUM_LOG_FILES, true); // Custom FileHandler Object to "log"
            logHandler.setFormatter(new SimpleFormatter());                         // Formatter object for plain text formatting of log entries
            LOGGER.addHandler(logHandler);                                          // Add the filehandler to the Logging object
        } catch (IOException e) {
            System.out.println("Couldn't create log file");                         // Tell the user if the log file couldn't be created
        }

        this.port = port;
        peersByAddress = new Hashtable<InetAddress, String>();                      // Create new empty objects for fields
        socketsByAddress = new Hashtable<InetAddress, Socket>();                    //              ''
        peerIPAddresses = new ArrayList<InetAddress>();                             //              ''
        unauthorisedIPAddresses = new ArrayList<InetAddress>();                     //              ''
        allMyIPAddresses = new ArrayList<InetAddress>();                            //              ''

        File peerFile = new File(peerFileName);                                     // New File for filename given to constructor
        String peerInfoString;

        try {
            Enumeration networkInterfaces = NetworkInterface.getNetworkInterfaces();    // Get all the local network interfaces
            while (networkInterfaces.hasMoreElements()) {                               // Loop over all of the above network interfaces
                NetworkInterface n = (NetworkInterface)networkInterfaces.nextElement();
                Enumeration ee = n.getInetAddresses();                                  // Get an enumerable for the addresses in the networkinterface
                while (ee.hasMoreElements())                                            // Loop over all addresses
                {
                    InetAddress i = (InetAddress) ee.nextElement();
                    allMyIPAddresses.add(i);                                            // Save as a local IP address
                }
            }
        } catch (SocketException e) {
            LOGGER.info("Socket Exception");                                            // Log on socket error
        }

        try {
            BufferedReader fileReader = new BufferedReader(new InputStreamReader(new FileInputStream(peerFile)));   // Obtain a BufferedReader for the peers file
            int lineNumber = 0;                                                                                     // Line number
            while ((peerInfoString = fileReader.readLine())!= null) {                                               // Read lines until there are none
                lineNumber++;                                                                                       // Iterate line number up
                String delims = "[,]";                                                                              // Delimit on commas
                String[] peerParts = peerInfoString.split(delims);                                                  // Split line read on delimiter
                if (peerParts.length == PEER_PARTS) {
                    String peerName = peerParts[0].trim();                                                          // Trim whitespace from string
                    InetAddress peerIP = InetAddress.getByName(peerParts[1].trim());                                // Parse string to IP after trimming whitespace

                    peersByAddress.put(peerIP, peerName);                                                           // Add new peer to collections
                    peerIPAddresses.add(peerIP);                                                                    // Add peer IP to collection of peer IP addresses
                } else {
                    System.err.println(peerFileName + " contained an error on line: " + lineNumber);                // Print error if there was a dodgy line in the text file, continue afterwards
                }
            }
        } catch (FileNotFoundException e) {
            System.err.println("Could not find peers.txt file");
            LOGGER.severe("Could not find peers.txt file");
            System.exit(1);
        } catch (IOException e ){
            System.err.println("Could not read from file");
            LOGGER.severe("Could not read from file");
            System.exit(1);
        }
    }

    /**
     * Get or create a socket for a given peer IP address
     * @param peer  The peer object to check for
     * @return  A socket to use
     * @throws IOException
     */
    public Socket checkOrAddSocket(InetAddress peer) throws IOException{
        Object lock = new Object();                         // An Object to lock
        Boolean isContained;                                // Boolean to signify if there's an active socket for this peer already
        Socket peerSocket;
        synchronized (lock) {                               // Lock to read
            if (socketsByAddress.contains(peer)){           // Check if there's a socket for the peer already
                isContained = true;                         // True if it is
            } else {
                isContained = false;                        // False if not
            }
        }                                                   // Unlock to allow other new connections to check concurrently
        if (isContained) {                                  // If it was contained
            synchronized (lock) {                           // Lock again for read
                peerSocket = socketsByAddress.get(peer);    // Get the socket
            }
            return peerSocket;                              // Return the socket
        } else {                                            // If there wasn't a socket
            peerSocket = new Socket(peer, port);            // Create a new one
            peerSocket.setKeepAlive(true);                  // Ask to keep connection alive

            synchronized (lock) {                           // Lock for write
                socketsByAddress.put(peer, peerSocket);     // Save the new socket for retrieval later
            }
            return peerSocket;                              // Return the retrieved socket
        }
    }

    /**
     * Send a message to all the known peers
     * @param message   String to send to peers
     */
    public void sendAll(final String message) {
        for (final InetAddress peer: peerIPAddresses) {                                 // For each valid peer IP Address

            Thread senderThread = new Thread(new Runnable() {                           // New thread to handle connection
                @Override
                public void run() {
                    try {
                        Socket peerSocket = checkOrAddSocket(peer);                     // Get socket for peer
                        PrintWriter netOutput = new PrintWriter(peerSocket.getOutputStream(), true); // Get a socket PrintWriter for output stream
                        netOutput.println(message);                                     // Send message as line (with '\n')
                    } catch (IOException e) {
                        LOGGER.info(e.getMessage() + ": " + peer.getHostAddress());
                    }
                }
            });
            senderThread.start();                                                       // Start thread
        }
    }

    /**
     * Check if the IP is this computer's IP
     * @param address   INetAddress to check
     * @return  True if it belongs to this computer
     */
    public boolean isMyAddress(InetAddress address) {
        if (allMyIPAddresses.contains(address)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Check if the IP is a valid IP from the peers file
     * @param address   INetAddress to check
     * @return  True if it is false otherwise
     */
    public boolean isValidIP(InetAddress address) {
        if (peerIPAddresses.contains(address)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the peer name for an IP
     * @param address   INetAddress to check
     * @return  Name of the peer as string
     */
    public String getName(InetAddress address) {
        return peersByAddress.get(address);         // Get and return the peer name
    }

    /**
     * Check if the system has already warned about this unauthorized IP address
     * @param address   INetAddress to check
     * @return  True if user already warned else false
     */
    public boolean unauthAlreadyWarned(InetAddress address) {
        if (unauthorisedIPAddresses.contains(address)) {
            return true;                            // If the IP exists it is unauth
        } else {
            unauthorisedIPAddresses.add(address);   // If it doesn't then add it
            return false;
        }
    }
}
